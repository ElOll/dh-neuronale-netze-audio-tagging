import matplotlib.pyplot as plt


def plot_accuracy(fitted_model, fileName):
    """

    :param fitted_model: neural network model after calling.fit
    :param fileName: name how the file should be saved
    :return:
    """
    # summarize history for accuracy
    plt.plot(fitted_model.history['acc'])
    plt.plot(fitted_model.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(fileName+'plot_accuracy.png', dpi=300, bbox_inches='tight')
    plt.show()


def plot_loss(fitted_model, fileName):
    """

    :param fitted_model: neural network model after calling.fit
    :param fileName: name how the file should be saved
    :return:
    """
    # summarize history for loss
    plt.plot(fitted_model.history['loss'])
    plt.plot(fitted_model.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(fileName+'plot_loss.png', dpi=300, bbox_inches='tight')
    plt.show()
