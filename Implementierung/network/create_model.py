from keras.layers import Bidirectional, GRU
from keras.layers.convolutional import Conv1D, MaxPooling1D
from keras.layers.core import Dense, Dropout, Activation
from keras.models import Sequential


def save_model_json(model, file_name, file_path):
    model_json = model.to_json()
    with open(file_path + file_name + ".json", "w") as json_file:
        json_file.write(model_json)
    model.save_weights(file_path + file_name + ".h5")


def create_model():
    # M (feadim) = feature dimension/dimension input/ number of samples/ M samples
    # du hast da vier definitionen die sich für mich nicht überschneiden da müssen wir nochmal überlegen.
    # ich glaube das M auf jedenfall die "sample (oder input feature) size" ist. das steht ja im Text aber dann macht number of samples keinen Sinn oder?
    M = 512

    # F = Filter/Kernel Size
    F = 400

    # N = Number of Filters
    N = 128

    # Calculation of the Pool Size = (Dimensions of the input) - (Number of Filters)
    pool_size = (M - F + 1)

    # nr of labels for sigmoid output layer
    n_out = 2

    # building the bidirectional convolutional gated recurrent unit model

    model = Sequential()

    # 1d Convolutional Neural Network
    # input shape: number of filters, feature dimension/number of samples
    # input_shape = (400, 512)
    model.add(Conv1D(filters=N, kernel_size=F, strides=1, padding='valid', use_bias=True, input_shape=(M, 1)))
    model.add(Activation('relu'))
    model.add(MaxPooling1D(pool_size=pool_size))
    # print('Model Summary after 1d CNN:')
    # model.summary()
    # print('\n\n')

    # 3 Bidirectional Gated Recurrent Units
    model.add(Bidirectional(GRU(units=128, return_sequences=True)))
    model.add(Bidirectional(GRU(units=128, return_sequences=True)))
    model.add(Bidirectional(GRU(units=128, return_sequences=False)))
    model.add(Dense(500))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    # print('Model Summary after the 3 Bidirectional GRU''s: ')
    # model.summary()
    # print('\n\n')

    # One Layer Dense Neural Network
    model.add(Dense(n_out))
    # print('Model Summary after the one layer DNN: ')
    # model.summary()
    # print('\n\n')

    # Sigmoid output layer
    model.add(Activation('sigmoid'))
    print('Complete Model after the sigmoid output layer: ')
    model.summary()
    print('\n\n')
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model
