import csv
import os
import shutil

import numpy as np
from keras.utils import to_categorical

tags = {'s', 't'}
tag_to_index = {lb: id for id, lb in enumerate(tags)}
index_to_tag = {id: lb for id, lb in enumerate(tags)}


def get_data_from_csv(file_name):
    """
    Loads csv file and appends all files to python list
    :param file_name: csv file name to be loaded
    :return python list of files
    """
    all_data_csv = []
    file_list = get_file_list(file_name)

    for file in file_list:
        all_data_csv.append(file)

    return all_data_csv


def get_file_list(file_name):
    """
    Transforms csv file in list
    :param file_name:
    :return: list of files
    """
    with open(file_name, 'rt') as generated_file:
        reader = csv.reader(generated_file)
        file_list = list(reader)
    return file_list


def save_numpy_file(name, data):
    """
    Saves file as .npy file
    :param name: name of file
    :param data: numpy array to be saved
    :return: name of saved file
    """
    np.save(name + '.npy', data)
    print("saved " + name + ".npy")
    return name + '.npy'


def load_data(csv_file):
    """
    loads csv data from file and saves in numpy arrays
    :param csv_file: csv file name
    :return: X,y numpy arrays with frames and tags
    """
    X = []
    y = []

    file_list = get_file_list(csv_file)

    for file in file_list:
        tag = file[1]
        print("file " + file[0])
        file = np.load(file[0])
        X.append(file)
        y.append(tag_to_index[tag])

    X = np.vstack(X)
    y = np.array(to_categorical(y))
    return X, y


def clear_csv_file(csv_file):
    """
    Deletes content of csv file
    :param csv_file: name of csv file
    """
    file = open(csv_file, "w+")
    file.close()


def clear_folder(folder_name):
    """
    Deletes content of folder
    :param folder_name: name of folder
    """
    shutil.rmtree(folder_name)
    os.mkdir(folder_name)


def create_folder(folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
