import random

import numpy as np

import data_generation.general_generation as general


def generate_chunks(number_of_files, duration, sample_rate, starting_point, folder, csv_file, parent_folder):
    """
    Generates audio numpy arrays as .npy as sinus and tangens curves and saves them
    :param number_of_files: number of audio chunks to be generated for sinus and tangens
    :param duration: duration of audio chunk in ms
    :param sample_rate: sample rate of audio chunk in kHz
    :param starting_point: starting point of generation of data, if 100 chunks exist starting point is 100
    :param folder: folder in which the chunks should be saved (must exist)
    :param csv_file: filename of csv file to save name and tag of chunk
    """
    general.create_folder(parent_folder + "/" + folder)
    # calculates number of data points of chunk as product of duration and sample_rate (e.g. 4000ms * 16 kHz = 64000)
    number_of_data_points = duration * sample_rate
    # loads csv with present chunks to avoid overwriting existing chunks
    all_chunks_csv = general.get_data_from_csv(parent_folder + "/" + csv_file)

    # generates tangens curves
    for n in range(number_of_files):
        # gets a random tangens curve with the number of data points inserted
        tanges_curve = generate_tangens_curve(number_of_data_points)
        # saves generated tangens curve in the given folder with the name 'tangens_ongoing_number)
        path = general.save_numpy_file(parent_folder + "/" + folder + '/tangens_' + str(starting_point + n),
                                       tanges_curve)
        # checks if tangens chunk is already created and adds it to list if not
        if not [path, 't'] in all_chunks_csv:
            all_chunks_csv.append([path, 't'])

    for n in range(number_of_files):
        sinus_curve = generate_sinus_curve(number_of_data_points)
        path = general.save_numpy_file(parent_folder + "/" + folder + '/sinus_' + str(starting_point + n), sinus_curve)
        if not [path, 's'] in all_chunks_csv:
            all_chunks_csv.append([path, 's'])

    # creates numpy array of all saved chunks list
    all_chunks_csv = np.array(all_chunks_csv)
    # saves numpy array as csv file e.g. in 'generated_chunks_train.csv'
    np.savetxt(parent_folder + "/" + csv_file, all_chunks_csv, fmt='%s', delimiter=',')


def generate_sinus_curve(number_of_data_points):
    random.seed()
    x = np.arange(0, number_of_data_points, 1)
    y_1 = random.uniform(0.01, 10) * np.sin(x / random.uniform(1, 10))
    y_2 = random.uniform(0.01, 10) * np.sin(x / random.uniform(1, 10))
    y_3 = random.uniform(0.01, 10) * np.sin(x / random.uniform(1, 10))

    return y_1 + y_2 + y_3


def generate_tangens_curve(number_of_data_points):
    random.seed()
    x = np.arange(0, number_of_data_points, 1)
    y_1 = random.uniform(0.009, 10) * np.tan(random.uniform(35, 50) * x)
    y_2 = random.uniform(0.009, 10) * np.tan(random.uniform(35, 50) * x)
    y_3 = random.uniform(0.009, 10) * np.tan(random.uniform(35, 50) * x)
    return y_1 + y_2 + y_3
