import data_generation.chunk_generator as chunk
import data_generation.frame_generator as frame
import data_generation.general_generation as general


def generate_sample_data_train_and_test(number_of_files, sample_rate, duration, frame_length, hop_size):
    chunk.generate_chunks(number_of_files=int(0.2 * number_of_files),
                          duration=duration,
                          sample_rate=sample_rate,
                          starting_point=0,
                          folder='sample_chunks_test',
                          csv_file='generated_chunks_test.csv', parent_folder='data_generation')

    frame.generate_frames(sample_rate=sample_rate,
                          frame_length=frame_length,
                          hop_size=hop_size,
                          folder='sample_frames_test',
                          csv_file_frames='generated_frames_test.csv',
                          csv_file_chunks='generated_chunks_test.csv', parent_folder='data_generation')

    chunk.generate_chunks(int(0.8 * number_of_files),
                          duration=duration,
                          sample_rate=sample_rate,
                          starting_point=0,
                          folder='sample_chunks_train',
                          csv_file='generated_chunks_train.csv', parent_folder='data_generation')
    frame.generate_frames(sample_rate=sample_rate,
                          frame_length=frame_length,
                          hop_size=hop_size,
                          folder='sample_frames_train',
                          csv_file_frames='generated_frames_train.csv',
                          csv_file_chunks='generated_chunks_train.csv', parent_folder='data_generation')


def generate_sample_data_eval(number_of_files, sample_rate, duration, frame_length, hop_size):
    chunk.generate_chunks(int(0.8 * number_of_files),
                          duration=duration,
                          sample_rate=sample_rate,
                          starting_point=0,
                          folder='sample_chunks_eval',
                          csv_file='generated_chunks_eval.csv', parent_folder='evaluation')
    frame.generate_frames(sample_rate=sample_rate,
                          frame_length=frame_length,
                          hop_size=hop_size,
                          folder='sample_frames_eval',
                          csv_file_frames='generated_frames_eval.csv',
                          csv_file_chunks='generated_chunks_eval.csv', parent_folder='evaluation')


def generate_sample_data_and_clean(number_of_files_train_test, number_of_files_eval, sample_rate, duration,
                                   frame_length, hop_size):
    general.clear_csv_file('data_generation/generated_chunks_test.csv')
    general.clear_csv_file('data_generation/generated_frames_test.csv')
    general.clear_folder('data_generation/sample_chunks_test')
    general.clear_folder('data_generation/sample_frames_test')
    general.clear_csv_file('data_generation/generated_chunks_train.csv')
    general.clear_csv_file('data_generation/generated_frames_train.csv')
    general.clear_folder('data_generation/sample_chunks_train')
    general.clear_folder('data_generation/sample_frames_train')
    general.clear_csv_file('evaluation/generated_chunks_eval.csv')
    general.clear_csv_file('evaluation/generated_frames_eval.csv')
    general.clear_folder('evaluation/sample_chunks_eval')
    general.clear_folder('evaluation/sample_frames_eval')
    generate_sample_data_train_and_test(number_of_files_train_test, sample_rate, duration, frame_length, hop_size)
    generate_sample_data_eval(number_of_files_eval, sample_rate, duration, frame_length, hop_size)
