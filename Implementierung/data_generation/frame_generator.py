import numpy as np

import data_generation.general_generation as general


def generate_frames(sample_rate, frame_length, hop_size, folder, csv_file_frames, csv_file_chunks, parent_folder):
    """
    Divides generated audio chunks in frames and saves them as .npy files
    :param parent_folder: parent folder of generation_file
    :param sample_rate: sample rate of audio chunk in kHz
    :param frame_length: frame length of audio frame in ms
    :param hop_size: hop size of audio frame in ms
    :param folder: folder in which the chunks should be saved (must exist)
    :param csv_file_frames: filename of csv file to save name and tag of frames
    :param csv_file_chunks: filename of csv file to save name and tag of chunk
    """
    general.create_folder(parent_folder + "/" + folder)
    # loads csv with all chunks generated
    file_list = general.get_file_list(parent_folder + "/" + csv_file_chunks)
    # loads csv with present chunks to avoid overwriting existing frames
    all_frames_csv = general.get_data_from_csv(parent_folder + "/" + csv_file_frames)
    # calculates number of data points for a frame as product of duration and sample_rate (e.g. 32ms * 16 kHz = 512)
    data_points_per_frame = frame_length * sample_rate
    # calculates number of data points for the hop size (e.g. 16ms * 16 kHz = 256)
    data_points_hop_size_per_frame = hop_size * sample_rate

    for file in file_list:
        # saves tag of chunks
        tag = file[1]
        # saves file_name, parts
        file_name = file[0].split('/')[2].split('.')[0]
        file_parts = file[0].split('/')
        # loads the chunk in variable chunk
        chunk = np.load(file[0])
        number = 0
        n = 0

        # iterates until end of chunk
        while n + data_points_per_frame < len(chunk):
            # calculates frame end as point where we are + data_points_per_frame
            frame_end = int(n + data_points_per_frame)
            # saves frame in frame folder
            path = general.save_numpy_file(parent_folder + "/" + folder + '/' + file_name + "_frame_" + str(number),
                                           chunk[n:frame_end])
            print("added frame from: " + str(n) + " to: " + str(frame_end))
            number += 1
            n += int(data_points_hop_size_per_frame)
            # adds frame name and tag to frame csv
            if not [path, tag] in all_frames_csv:
                all_frames_csv.append([path, tag])
                print("appended " + path + " to csv")

    # creates numpy array of all saved frames list
    all_frames_csv = np.array(all_frames_csv)
    # saves numpy array as csv file e.g. in 'generated_frames'
    np.savetxt(parent_folder + "/" + csv_file_frames, all_frames_csv, fmt='%s', delimiter=',')
    print('saved all_frames_csv')
