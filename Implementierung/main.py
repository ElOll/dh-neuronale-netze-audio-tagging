from keras.callbacks import ModelCheckpoint

import data_generation.general_generation as gen
import data_generation.generate as generation
import evaluation.eval_model as eval
import network.model_visualizer as mv
from network.create_model import create_model


def reshape_x(x, number_of_data_points):
    length = len(x)
    return x.reshape((length, number_of_data_points, 1))


if __name__ == '__main__':
    # when running the programm for the first time do not use the function generation.generate_sample_data_and_clean() but the generation.generate_sample_data... instead
    # only when running the programm the second time, and one wants to create new data the generation.generate_sample_data_and_clean() can be used
    number_of_files_train_test = 1000
    number_of_files_eval = 100
    number_of_files = 1000
    sample_rate = 16
    duration = 4000
    frame_length = 32
    hop_size = 16

    #generation.generate_sample_data_and_clean(number_of_files_train_test=number_of_files_train_test, number_of_files_eval=number_of_files_eval, sample_rate=sample_rate,duration=duration, frame_length=frame_length, hop_size=hop_size)
    generation.generate_sample_data_train_and_test(number_of_files=number_of_files, sample_rate= sample_rate, duration= duration, frame_length=frame_length, hop_size= hop_size) 
    generation.generate_sample_data_eval(number_of_files=number_of_files_eval, sample_rate=sample_rate,duration=duration, frame_length=frame_length,hop_size=hop_size)

    generation.generate_sample_data_and_clean(number_of_files_train_test=10, number_of_files_eval=5, sample_rate=16,
                                              duration=4000, frame_length=32, hop_size=16)
    tr_X, tr_y = gen.load_data('data_generation/generated_frames_train.csv')
    tr_X = reshape_x(tr_X, 512)

    val_X, val_y = gen.load_data('data_generation/generated_frames_test.csv')
    val_X = reshape_x(val_X, 512)

    model = create_model()
    model_name = "test_model"
    model_callback = ModelCheckpoint("evaluation/saved_models/" + model_name + ".hdf5", monitor='val_loss', verbose=0,
                                     save_best_only=False, save_weights_only=False, mode='auto')
    fitted_model = model.fit(tr_X, tr_y, batch_size=62, epochs=2, verbose=1, validation_data=(val_X, val_y),
                             callbacks=[model_callback])
    eval.eval_model(path="evaluation/saved_models/", file_name=model_name + ".hdf5", threshold=0.4)
    mv.plot_accuracy(fitted_model=fitted_model, fileName=model_name)
    mv.plot_loss(fitted_model=fitted_model, fileName=model_name)
