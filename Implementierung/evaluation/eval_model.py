import numpy as np
from keras.models import load_model

import data_generation.general_generation as generation
import evaluation.eer as eer

tags = {'s', 't'}
tag_to_index = {lb: id for id, lb in enumerate(tags)}
index_to_tag = {id: lb for id, lb in enumerate(tags)}


def one_hot_encode_tag(tag):
    y = np.zeros(len(tags))
    for ch in tag:
        y[tag_to_index[ch]] = 1
    return y


def reshape_x(x, number_of_data_points):
    return x.reshape((1, number_of_data_points, 1))


def eval_model(path, file_name, threshold):
    y_true_binary_s = []
    y_true_file_s = []
    y_true_binary_t = []
    y_true_file_t = []
    result_list = []

    model = load_model(path + file_name)

    file_list = generation.get_file_list('evaluation/generated_frames_eval.csv')

    for file in file_list:
        tag = file[1]
        one_hot_encoded_tag = one_hot_encode_tag(tag)
        file_path = file[0]
        frame = reshape_x(np.load(file_path), 512)
        p_y_pred = model.predict(frame)
        p_y_pred = np.mean(p_y_pred, axis=0)
        pred = np.zeros(len(tags))
        pred[np.where(p_y_pred > threshold)] = 1
        ind = 0
        for tag in tags:
            if tag == 't':
                y_true_file_t.append(file_path)
                y_true_binary_t.append(one_hot_encoded_tag[ind])
            elif tag == 's':
                y_true_file_s.append(file_path)
                y_true_binary_s.append(one_hot_encoded_tag[ind])
            result = [file_path, tag, p_y_pred[ind]]
            result_list.append(result)
            print("added result: " + str(result))
            ind += 1
    result_list = np.array(result_list)
    np.savetxt("result.csv", result_list, fmt='%s', delimiter=',')

    dict_s = dict(zip(y_true_file_s, y_true_binary_s))
    dict_t = dict(zip(y_true_file_t, y_true_binary_t))

    eer_s = eer.compute_eer('result.csv', 's', dict_s)
    eer_t = eer.compute_eer('result.csv', 't', dict_t)
    eer_mean = (eer_s + eer_t) / 2
    print("EER: " + str(eer_mean))
